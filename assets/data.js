
import React from 'react';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const  services = [
    {
        id : '1',
        name : 'Car/Bus Hire',
        icon :<FontAwesome5 style={{fontSize:20,color:'red'}} name="bus" />,
    },
    {
        id : '2',
        name : 'TOTO',
        icon :<FontAwesome5 style={{fontSize:20,color:'red'}} name="shuttle-van" />,
    },
  
];

export default services;
