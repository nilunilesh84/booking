import React from 'react';
import { useEffect } from 'react';
import AppNavigation from './src/AppNavigation';


import PushNotification, { Importance } from 'react-native-push-notification';

import ForegroundHandler from './src/Notification/ForegroundHandler';
import {
  notificationListner,
  requestUserPermission,
} from './src/Notification/Notifications';
export default function App() {
  useEffect(() => {
    PushNotification.createChannel(
      {
        channelId: "channel-id", // (required)
        channelName: "My channel", // (required)
        channelDescription: "A channel to categorise your notifications", // (optional) default: undefined.
        playSound: false, // (optional) default: true
        soundName: "default", // (optional) See `soundName` parameter of `localNotification` function
        importance: Importance.HIGH, // (optional) default: Importance.HIGH. Int value of the Android notification importance
        vibrate: true, // (optional) default: true. Creates the default vibration pattern if true.
      },
      (created) => console.log(`createChannel returned '${created}'`) // (optional) callback returns whether the channel was created, false means it already existed.
    );
    try {
      PushNotification.localNotificationSchedule({
        //... You can use all the options from localNotifications
        message: "My Notification Message", // (required)
        channelId: 'channel-id',
        date: new Date(Date.now() + 10 * 1000), // in 60 secs
        allowWhileIdle: false, // (optional) set notification to work while on doze, default: false
      
        /* Android Only Properties */
        repeatTime: 1, // (optional) Increment of configured repeatType. Check 'Repeating Notifications' section for more info.
      });
      // PushNotification.localNotification({
      //   /* Android Only Properties */
      //   channelId: 'channel-id',
      //   color: 'green',
      //   smallIcon: '',
      //   bigLargeIconUrl: "https://sayas-public.s3.amazonaws.com/1653478095191images(5).jpeg",
      //   largeIconUrl: "https://sayas-public.s3.amazonaws.com/1653478095191images(5).jpeg",
      //   vibrate: true, // (optional) default: true
      //   vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000
      //   title: 'notification', // (optional)
      //   message: 'notification', // (required)
      //   body: ' notification', // (required)
      //   picture: 'https://www.bing.com/th?id=ORMS.4ccef3966920e3b653a35d4b64b77504&pid=Wdp&w=300&h=225&qlt=60&c=1&rs=1&dpr=0.9749999642372131&p=0'
      // });
    } catch (error) {
      console.log(error)
    }
    PushNotification.deleteChannel("fcm_fallback_notification_channel")
    PushNotification.getChannels(function (channel_ids) {
      console.log(channel_ids); // ['channel_id_1'] 
    });
  }, [])
  useEffect(() => {
    requestUserPermission();
    notificationListner();
  }, []);

  return (
    <>
      <ForegroundHandler />
      <AppNavigation />
    </>
  );
}

