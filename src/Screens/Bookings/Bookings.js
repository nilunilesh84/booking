/* eslint-disable prettier/prettier */
import React, { useState } from 'react';
import { View, Text, TextInput, StyleSheet } from 'react-native';

export default function Bookings() {
    const [input, setInput] = useState('')
    const [inputpassword, setInputpassword] = useState('')
    console.log(input)
    return (
        <View style={styles.container}>
            <TextInput style={styles.input} placeholder='enter name' data={input} type='text' onChange={value => setInput(value)} />
            <TextInput style={styles.input} placeholder='enter password' data={inputpassword} type='text' onChange={value => setInputpassword(value)} />
        </View>
    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    input: {
        width: 250,
        marginTop: 35,
        // padding: 8,
        borderRadius:5,
        borderWidth: 1,
        fontSize: 18
    }
})
