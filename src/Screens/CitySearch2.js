import { StyleSheet, Text, View } from 'react-native';
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { TextInput } from 'react-native-paper';
import {  destinationSave } from '../redux/Actions/action';
import { NAVIGATION } from '../Constants/navigations';

const CitySearch2 = ({ navigation, placeholder }) => {
  const [destination, setDestination] = useState('')
  const dispatch = useDispatch()
  return (
    <View >
      <TextInput style={styles.input}
        onEndEditing={() => (dispatch(destinationSave(destination)), navigation.navigate(NAVIGATION.HOME))}
        placeholderTextColor="gray" placeholder='ENTER DESTINATION'
        value={destination} onChangeText={(value) => setDestination(value)} />
    </View>
  );
};

export default CitySearch2;

const styles = StyleSheet.create({

});
