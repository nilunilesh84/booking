import { View, Text, FlatList, TextInput } from 'react-native'
import React, { useRef, useState } from 'react'
import { Button, Checkbox } from 'react-native-paper'

const WelcomeScreen = () => {

    const [selected, setselected] = useState([])
    const [editselected, seteditselected] = useState()
    const [editstate, seteditstate] = useState(false)
    const [str, setstr] = useState('')
    const [users, setusers] = useState([
        { name: 'nilesh', id: 1 },
        { name: 'body', id: 2 },
    ])
    const textref = useRef()
    const deleteuser = (id) => {
        if (id) {
            const filt = users.filter(ite => ite.id !== id)
            setusers(filt)
        } else {
            setusers([])
        }
    }
    const addusers = () => {
        if (editstate && str) {
            const dat = users.map(item => item.id == editselected ? { ...item, name: str } : item)
            setusers(dat)
            setstr('')
            seteditstate(false)
        } else {
            if (str) {
                const data = [...users, { name: str, id: Math.random() }]
                setusers(data)
                setstr('')
            } else {
                alert('fill')
            }
        }
    }
    console.log(textref.current)
    const editUsers = (item, i) => {
        setstr(item)
        seteditstate(true)
        seteditselected(i)
    }
    const deleteselected = () => {
        const data = users.filter(item => !selected.includes(item.id))
        setusers(data)
    }
    const renderItem = ({ item }) => {
        return (
            <View style={{ marginVertical: 12, flexDirection: 'row', width: '80%', alignItems: 'center', justifyContent: 'space-between' }}>
                <Checkbox
                    status={selected.includes(item.id) ? 'checked' : "unchecked"}
                    onPress={() => {
                        if (selected.includes(item.id)) {
                            const filt = selected.filter((ite) => ite !== item.id)
                            setselected(filt)
                        } else {
                            setselected([...selected, item.id])
                        }
                    }}
                />
                <Text onPress={() => editUsers(item.name, item.id)}>{item.name}</Text>
                <Button onPress={() => deleteuser(item.id)}>delete</Button>
            </View>
        )
    }
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', padding: 15 }}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <TextInput ref={textref} value={str} onChangeText={(tex) => setstr(tex)} style={{ borderColor: 'black', borderWidth: 2, width: '70%' }} />
                <Button onPress={() => addusers()} mode='contained'>{!editstate ? 'Add' : 'update'}</Button>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '100%' }}>
                <Text onPress={() => deleteselected()}>Delete Selected</Text>
                <Text onPress={() => setselected(users.map(ite => ite.id))}>Select all</Text>
                <Text onPress={() => deleteuser()}>Delete All</Text>
            </View>
            <FlatList
                data={users}
                renderItem={renderItem}
                keyExtractor={i => i.id}
            />
        </View>
    )
}

export default WelcomeScreen





// import React, { useEffect, useState } from 'react'
// import { FlatList, StyleSheet, Text, TextInput, View } from 'react-native'
// import { Dialog } from 'react-native-paper'

// const WelcomeScreen = () => {
//     const data = ['apple', 'boy', 'cat']
//     const [first, setfirst] = useState('')
//     const [arr, setarr] = useState()
//     const filterdata = (ite) => {
//         const da = data.filter(item => item.toLowerCase().includes(ite.toLowerCase()))
//         setarr(da)
//         if (da.length && ite) {
//             setfirst('error')
//         } else {
//             setfirst('')
//         }
//     }
//     const renderItem = ({ item }) => {
//         return (
//             <View>
//                 <Text>{item}</Text>
//             </View>
//         )
//     }
//     useEffect(() => {
//         setarr(data)
//     }, [])
//     return (
//         <View>
//             <Text>WelcomeScreen</Text>
//             <TextInput
//                 style={{ borderColor: 'blue', borderWidth: 2 }}
//                 onChangeText={t => filterdata(t)} />
//             <Text style={{ color: 'red' }}>{first}</Text>
//             <FlatList
//                 data={arr}
//                 renderItem={item => renderItem(item)} />
//         </View>
//     )
// }

// export default WelcomeScreen



// import { Button, FlatList, StyleSheet, Text, TextInput, View } from 'react-native'
// import React, { useState } from 'react'
// import { Checkbox } from 'react-native-paper'

// const WelcomeScreen = () => {
//     const [first, setfirst] = useState([
//         { name: 'nilu', id: 1 },
//         { name: 'boby', id: 2 },
//     ])
//     const [str, setstr] = useState('')
//     const [id, setId] = useState()
//     const [selected, setselected] = useState([])

//     const [edit, setEdit] = useState(false)
//     const renderItem = ({ item }) => {
//         return (
//             <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '100%' }}>
//                 <Checkbox
//                     onPress={() => {
//                         if (selected.includes(item.id)) {
//                             const fill = selected.filter(ite => ite !== item.id)
//                             setselected(fill)
//                         } else {
//                             setselected([...selected, item.id])
//                         }
//                     }
//                     }
//                     status={selected.includes(item.id) ? 'checked' : 'unchecked'} />
//                 <Text onPress={() => ediTodo(item)} style={{ width: '80%' }}>
//                     {item.name}
//                 </Text>
//                 <Button style={{ width: '20%' }} title='delete' onPress={() => deleteTodo(item.selected)} />
//             </View>
//         )
//     }
//     const addTodo = () => {
//         if (edit && id) {
//             const editd = first.map(ite => ite.id == id ? { ...ite, name: str } : ite)
//             setstr('')
//             setfirst(editd)
//             setEdit(false)
//         } else {
//             const add = [...first, { name: str, id: Math.random() }]
//             setfirst(add)
//             setstr('')
//         }
//     }
//     const deleteTodo = (i) => {
//         const dele = first.filter(ite => ite.id !== i)
//         setfirst(dele)
//     }
//     const ediTodo = (e) => {
//         setEdit(true)
//         setstr(e.name)
//         setId(e.id)
//     }
//     return (
//         <View>
//             <Text>WelcomeScreen</Text>
//             <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '100%' }}>
//                 <TextInput value={str} style={{ borderColor: 'blue', borderWidth: 2, width: '80%' }}
//                     onChangeText={t => setstr(t)} />
//                 <Button style={{ width: '20%' }} title={edit ? 'edit' : 'add'} onPress={addTodo} />
//             </View>
//             <FlatList
//                 data={first}
//                 renderItem={(item) => renderItem(item)} />
//             <Button style={{ width: '20%' }} title={'delete selected'} onPress={() => {
//                 const filters = first.filter(ite => !selected.includes(ite.id))
//                 setfirst(filters)
//             }} />
//             <Button style={{ width: '20%' }} title={'delete all'} onPress={() => {
//                 setfirst([])
//             }} />
//             <Button style={{ width: '20%' }} title={'Select all'} onPress={() => {
//                 const da = first.map(it => it.id)
//                 setselected(da)
//             }} />
//         </View>
//     )
// }

// export default WelcomeScreen

// const styles = StyleSheet.create({})