import React, { useState, useEffect } from 'react';
import {
  Text,
  View,
  Dimensions,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { ProgressChart } from 'react-native-chart-kit'
import colors from '../assets/colors/colors';
import LinearGradient from 'react-native-linear-gradient';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Loader from '../components/loader';
import MaterialIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { getDataAcquisitionsResponsesofamember, getAcquistionDataApi, getUserDetailsByMemberId, } from '../../constants';
import DialogLanding from '../components/DialogLanding';
import { Avatar } from 'react-native-paper';

const { width, height } = Dimensions.get('window');


function LandingScreen({ navigation }) {
  const [visible, setVisible] = React.useState(false);
  const [choose, setchoose] = React.useState('');
  const chartConfig = {
    backgroundGradientFrom: "#FFFF",
    backgroundGradientFromOpacity: 2,
    backgroundGradientTo: "#FFFF",
    color: (opacity = 1) => `rgba(80, 72, 229, ${opacity})`,
    strokeWidth: 3,
  };

  const [loading, setLoading] = useState(false);
  const [loaderMessage, setLoaderMessage] = useState('');
  const [userDetails, setuserDetails] = useState(null);
  const [dataAcquisition, setdataAcquisition] = useState(null);
  const [applicationsarray, setapplicationsarray] = useState();
  const [pollsarrayfileterd, setpollsarrayfileterd] = useState([]);
  const [surveysarray, setsurveysarray] = useState();
  const [pollsarray, setpollsarray] = useState();
  const [dataAcquisitionsResponses, setdataAcquisitionsResponses] = useState(null);
  useEffect(() => {
    getdataAcquistionApiCall()
  }, [])
  const logoutAndClearAsyncStorage = async () => {
    await AsyncStorage.clear();
    navigation.navigate('LoginStack', { screen: 'LoginScreen' });
  };

  const getdataAcquistionApiCall = async () => {
    setLoading(true);
    setLoaderMessage('loading...');
    try {
      // let notificationresponse = await getNotificationsforMember()
      let datareseponse = await getAcquistionDataApi();
      let datareseponseuser = await getUserDetailsByMemberId();
      let dataAcquisitionsResponse = await getDataAcquisitionsResponsesofamember();
      setdataAcquisitionsResponses(dataAcquisitionsResponse)
      setdataAcquisition(datareseponse);
      setuserDetails(datareseponseuser)
      // setnotification(notificationresponse)
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }

  };
  useEffect(() => {
    let findAppicationlength = dataAcquisition?.applications.map(item => item.questions)
    let findSurveyslength = dataAcquisition?.surveys.map(item => item.questions)
    let findpollslength = dataAcquisition?.polls.map(item => item.questions)
    let emptyaplica = []
    let emptysurveys = []
    let emptypolls = []
    findAppicationlength?.map(item => {
      if (Array.isArray(item)) {
        const a = item.map(ele => ele.id)
        emptyaplica = emptyaplica.concat(a)
        return a
      }
      return item
    })
    findSurveyslength?.map(item => {
      if (Array.isArray(item)) {
        const a = item.map(ele => ele.question)
        emptysurveys = emptysurveys.concat(a)
        return a
      }
      return item
    })
    findpollslength?.map(item => {
      if (Array.isArray(item)) {
        const a = item.map(ele => ele.id)
        emptypolls = emptypolls.concat(a)
        return a
      }
      return item
    })
    const applicationspercentage = dataAcquisitionsResponses?.applications[0]?.response.length / emptyaplica.length
    const surveyspercentage = dataAcquisitionsResponses?.surveys[0].response.length / emptysurveys.length
    const pollspercentage = dataAcquisitionsResponses?.polls[0]?.response.length / emptypolls.length
    setapplicationsarray(applicationspercentage)
    setpollsarray(pollspercentage)
    setsurveysarray(surveyspercentage)

    const answerspolls = dataAcquisitionsResponses?.polls[0]?.response.map(item => item.id)
   
    let arr = []

    findpollslength?.forEach(item => {
      arr = arr?.concat(item)
    })

    arr = arr.map(item => {
      if (answerspolls.includes(item.id)) return { ...item, answered: true }
      return item
    })
    setpollsarrayfileterd(arr)
     console.log(arr)



  }, [dataAcquisition?.applications])
console.log({pollsarrayfileterd})
  const data = {
    labels: ["Applications", "Surveys", "Polls"],
    data: [applicationsarray || 0, surveysarray || 0, pollsarray || 0]
  };

  const applicationsFlatlist = (item) => {
    return (
      <TouchableOpacity onPress={() => (setVisible(true), setchoose(item))} style={{ width: 150 }}>
        <View
          style={{
            backgroundColor: 'white',
            marginHorizontal: 10,
            margin: 10,
            elevation: 10,
            borderRadius: 10,
          }}>
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              marginTop: 10,
              marginLeft: 10,
              justifyContent: 'space-between'
            }}>
            <Text style={{ fontSize: 10, fontWeight: '600' }}>
              {item.title}
            </Text>
            <View style={{ marginLeft: 10 }}>
              <Image
                source={{ uri: item?.attachmentUrl }}
                resizeMode={'contain'}
                style={{ width: 10, height: 10 }}
              />
            </View>
          </View>
          <View style={{ marginTop: 5, marginBottom: 5, marginLeft: 10 }}>
            <Text
              style={{
                fontSize: 7,
                fontWeight: '600',
                color: colors.flatListApplication
              }}>
              {item?.description}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <SafeAreaView style={styles.container}>
      <Loader loading={loading} message={loaderMessage} />
      <View style={{ backgroundColor: colors.headerBlue }}>
        <View
          style={{
            backgroundColor: colors.headerBlue,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: 20,
          }}>
          <View
            style={{
              justifyContent: 'flex-start',
              margin: 10,
              alignSelf: 'flex-start',
              flexDirection: 'row',
              marginTop: 10,
            }}>
            <TouchableOpacity style={{ paddingHorizontal: 8 }} onPress={() => navigation.openDrawer()}>
              {
                userDetails?.userImage ?
                  <Avatar.Image size={44} source={{ uri: userDetails?.userImage }} /> :
                  <Avatar.Icon style={{ backgroundColor: 'purple' }} size={44} />
              }
            </TouchableOpacity>
          </View>
          <View
            style={{
              justifyContent: 'flex-start',
              marginTop: 10,
              alignSelf: 'flex-start',
              flexDirection: 'row',
            }}>
            <TouchableOpacity style={{ alignItems: 'center', marginRight: 15 }}
              onPress={() => navigation.navigate('NotificationScreen')}>
              {/* <Avatar.Text style={{zIndex:100,position:'absolute'}} size={24} label={notification?.length} /> */}
              <Ionicons
                name="notifications-outline"
                size={25}
                color={'white'}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => logoutAndClearAsyncStorage()}>
              <View>
                <MaterialIcons
                  name="logout"
                  size={25}
                  color={'white'}
                  style={{ marginRight: 10 }}
                />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View
        style={{
          backgroundColor: colors.portfolioBackgroundBlue,
          borderBottomRightRadius: 100,
          borderBottomLeftRadius: 100,
          height: '30%',
        }}>
        <View
          style={{
            height: '90%',
            marginTop: 40,
            elevation: 20,
            backgroundColor: 'white',
            marginHorizontal: 20,
            borderRadius: 20,
            alignItems: 'center',
            overflow: 'hidden',
            justifyContent: 'center',
          }}>
          <ProgressChart
            data={data}
            width={width * .9}
            height={120}
            radius={20}
            strokeWidth={7}
            chartConfig={chartConfig}
            style={{ right: 42 }}
          />
          <View style={{
            justifyContent: 'space-between',
            flexDirection: 'row',
            paddingHorizontal: 32,
            width: '100%'
          }}>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ color: '#5048E5' }}>Applications</Text>
              <Text style={{ fontSize: 26 }}>{dataAcquisition?.applications.map(item => item.questions)?.length || '0'}</Text>
            </View>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ color: '#5048E5' }}>Surveys</Text>
              <Text style={{ fontSize: 26 }}>{dataAcquisition?.surveys.map(item => item.questions)?.length || '0'}</Text>
            </View>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ color: '#5048E5' }}>Polls</Text>
              <Text style={{ fontSize: 26 }}>{dataAcquisition?.polls.map(item => item.questions)?.length || '0'}</Text>
            </View>
          </View>
        </View>
      </View>
      <ScrollView>
        <View style={{ marginBottom: 60 }}>
          <View style={{ flex: 1, marginHorizontal: 20, marginTop: 40 }}>
            <LinearGradient
              start={{ x: 1.0, y: 0.5 }}
              end={{ x: 0.7, y: 1.0 }}
              colors={['rgba(71, 123, 255, 1)', 'rgba(110, 175, 236, 1)']}
              style={styles.linearGradient}>
              <View
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View>
                  <Text
                    style={[
                      styles.buttonText,
                      { fontWeight: '600', fontSize: 14 },
                    ]}>
                    Applications
                  </Text>
                </View>
                <TouchableOpacity
                  onPress={() => navigation.navigate('AcquisitionScreen', { props: 'application' })}
                >
                  <View style={{ justifyContent: 'center', alignSelf: 'center' }}>
                    <Text
                      style={[
                        styles.buttonText,
                        { fontWeight: '600', fontSize: 9 },
                      ]}>
                      See more
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={{ width: '100%' }}>
                <FlatList
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  data={dataAcquisition?.applications}
                  renderItem={item => applicationsFlatlist(item.item)}
                  keyExtractor={(item, index) => index}
                />
              </View>
            </LinearGradient>
          </View>
          <View style={{ flex: 1, marginHorizontal: 20, marginTop: 40 }}>
            <LinearGradient
              start={{ x: 1.0, y: 0.5 }}
              end={{ x: 0.7, y: 1.0 }}
              colors={['rgba(71, 123, 255, 1)', 'rgba(110, 175, 236, 1)']}
              style={styles.linearGradient}>
              <View
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View>
                  <Text
                    style={[
                      styles.buttonText,
                      { fontWeight: '600', fontSize: 14 },
                    ]}>
                    Surveys
                  </Text>
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('AcquisitionScreen', { props: 'survey' })} style={{ justifyContent: 'center', alignSelf: 'center' }}>
                  <Text
                    style={[
                      styles.buttonText,
                      { fontWeight: '600', fontSize: 9 },
                    ]}>
                    See more
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{ width: '100%' }}>
                <FlatList
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  data={dataAcquisition?.surveys}
                  renderItem={(item) => applicationsFlatlist(item.item)}
                  keyExtractor={(item, index) => index}
                />
              </View>
            </LinearGradient>
          </View>
          <View style={{ flex: 1, marginHorizontal: 20, marginTop: 40 }}>
            <LinearGradient
              start={{ x: 1.0, y: 0.5 }}
              end={{ x: 0.7, y: 1.0 }}
              colors={['rgba(71, 123, 255, 1)', 'rgba(110, 175, 236, 1)']}
              style={styles.linearGradient}>
              <View
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View>
                  <Text
                    style={[
                      styles.buttonText,
                      { fontWeight: '600', fontSize: 14 },
                    ]}>
                    Polls
                  </Text>
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('AcquisitionScreen', { props: 'polls' })} style={{ justifyContent: 'center', alignSelf: 'center' }}>
                  <Text
                    style={[
                      styles.buttonText,
                      { fontWeight: '600', fontSize: 9 },
                    ]}>
                    See more
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{ width: '100%' }}>
                <FlatList
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  data={dataAcquisition?.polls}
                  renderItem={(item) => applicationsFlatlist(item.item)}
                  keyExtractor={(item, index) => index}
                />
              </View>
            </LinearGradient>
          </View>
        </View>
      </ScrollView>
      <DialogLanding dataAcquisition={dataAcquisition} choose={choose} visible={visible} setVisible={setVisible} />
    </SafeAreaView>
  );
}
export default LandingScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    height: height,
  },
  bottomBorder: {
    backgroundColor: colors.lightgray,
    height: 0.5,
    marginTop: 15,
  },
  linearGradient: {
    // flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5,
    // borderRadius: 6,
  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
});
