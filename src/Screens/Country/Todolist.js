import React, { useState } from 'react'
import { Text, View, StyleSheet, TextInput, TouchableOpacity } from 'react-native'
import { Button } from 'react-native-paper'
import { NAVIGATION } from '../../Constants/navigations'

const Country = ({ navigation }) => {

    const [value, setvalue] = useState('')
    const [arr, setarr] = useState([])
    const [editiditem, seteditiditem] = useState(null)

    const addTodo = ()=>{
        if(!value){
        alert('fill')
        }else if(value && editiditem){
               const filterda = arr.map(elem=>(
                   elem.id === editiditem ? {...elem,item:value} : elem
               ))
               setarr(filterda)
               seteditiditem(null)
        }
        else{
            const data = {id:Math.random(),item:value}
            setarr([data,...arr])
        }
        
    }
    const deleteTodo = (i)=>{
        const data= arr.filter((item,index)=> i !== index)
        setarr(data)
    }
  const editTodo = (i)=>{
      const data=arr.find((item)=>(
          item.id==i 
      ))
      setvalue(data.item)
      seteditiditem(i)
  }
 
    return (
        <View style={styles.body}>
            <View style={styles.wrapper}>
                <View>
                    <Text style={styles.heading}>Country </Text>
                    <TextInput onChangeText={text=>setvalue(text)} value={value} style={styles.input} />
                </View>
                {
                arr?.map((item,i)=>
                <View key={i} style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                    <Text onPress={()=>editTodo(item.id)}>{item.item}</Text>
                    <Button onPress={()=>deleteTodo(i)}>Delete</Button>
                </View>
                )
            }
            </View>
                     <View>
                <TouchableOpacity onPress={() => (addTodo(),setvalue('') )}>
                    <View style={{
                        backgroundColor: 'red',
                        alignItems: 'center',
                        justifyContent: 'center',
                        height: 45,
                        width: 400
                    }}
                    >
                        <Text style={{ color: 'white', fontWeight: '600', fontSize: 19 }}>{editiditem? 'Save' : 'Add'}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    body: {
        flex: 1,
        alignItems: 'center'
    },
    wrapper: {
        flex: 1,
        width: 200,
        alignItems: 'center'
    },
    input: {
        borderWidth: 1,
        width: 350,
        borderColor: 'black',
        fontSize: 18
    },
    heading: {
        fontWeight: '500',
        fontSize: 20,
        padding: 5
    },
    btn: {
    },
    btnstyle: {
        backgroundColor: 'red',
        padding: 10,
        width: 300,
    }
})

export default Country
// import { StyleSheet, Text, View, Platform, Button, TextInput, Pressable } from 'react-native'
// import React, { useState } from 'react'
// import DateTimePicker from '@react-native-community/datetimepicker';


// const LandingScreen = () => {
//   const [date, setDate] = useState(new Date(1598051730000));
//   const [mode, setMode] = useState('date');
//   const [show, setShow] = useState(false);
//   const [str, setstr] = useState('');
//   const [edit, setedit] = useState();
//   const [data, setdata] = useState([
//     { question: 1, },
//     { question: 2, },
//     { question: 3, }
//   ]);
//   const [todo, settodo] = useState([
//     { name: 'question', key: 1 },
//     { name: 'question2', key: 1 },
//     { name: 'question3', key: 1 },
//   ]);
//   const onChange = (selectedDate, item) => {
//     const currentDate = selectedDate || date;
//     setShow(Platform.OS === 'ios');
//     setDate(currentDate);
//     // setdata([...data, data.question === item ? { ...data.question, answer: currentDate }])
//   };
//   // console.log(data)

//   const addTodo = (ite,ind) => {
//     settodo(todo.map((item, i) => i === ind ? { ...item, answer: ite } : item))
//   }
//   const editTodo = (item, i) => {
//     setstr(item)
//     setedit(i)
//   }
//   console.log(todo)
//   return (
//     <View>
//       <TextInput style={{ borderColor: 'red', borderWidth: 4 }} value={str} onChangeText={(t) => setstr(t)} />
//       <Button title='add' onPress={addTodo} />
//       <View>
//         {
//           todo.map((item, i) => (
//             <Pressable style={{ marginVertical: 12, flexDirection: 'row' }} onPress={() => editTodo(item.name, i)} key={i}>
//               <Text>{item.name}</Text>
//               <TextInput style={{ width: 200, borderColor: 'red', borderWidth: 4 }} value={str} onChangeText={(t) => addTodo(t,i)} />
//             </Pressable>
//           ))
//         }
//       </View>
//     </View>
//   )
// }

// export default LandingScreen

// const styles = StyleSheet.create({})
// import React from 'react'

// import { addTodo, deleteTodo, handleChange, handleEdit, toggleEdit } from 'Action'
// import { connect } from 'react-redux'

// function Pra({ state, deleteTodo, toggleEdit, addTodo, handleChange, handleEdit }) {
 
//     console.log(state)
//     return (
//         <div>
//             <input type='text' value={state.str} onChange={e=>handleChange(e.target.value)} />
//             <button onClick={addTodo}>add</button>
//             {
//                 state.list.map((item,i)=>(
//                     <div key={i}>
//                         {
//                             item.edit ? 
//                             <input type='text' value={item.name} onChange={e=>handleEdit(e.target.value,i)}/>
//                             : <p>{item.name}</p>
//                         }
//                         <button onClick={()=>toggleEdit(i)}>{item.edit ? 'save' :'edit'}</button>
//                         <button onClick={()=>deleteTodo(i)}>delete</button>
//                     </div>
//                 ))
//             }
//         </div>
//     )
// }
// const mapstatetoProps = state => ({
//     state: state.todo
// })
// const mapdispatchtoProps = dispatch => ({
//     handleChange: i => dispatch(handleChange(i)),
//     addTodo: () => dispatch(addTodo()),
//     handleEdit: (e,i) => dispatch(handleEdit(e,i)),
//     toggleEdit: data => dispatch(toggleEdit(data)),
//     deleteTodo: data => dispatch(deleteTodo(data))
// })

// export default connect(mapstatetoProps, mapdispatchtoProps)(Pra);


   // const [state, setstate] = useState({
    //     str:'',
    //     list:[{name:'nilu',edit:false}]
    // })

    // const handleChange=(e)=>setstate({...state,str:e.target.value})
    // const addTodo=()=>{
    //     const data=[...state.list,{name:state.str,edit:false}]
    //     setstate({...state,list:data})
    // }
    // const handleEdit=(e,i)=>{
    //     const data=state.list.map((item,index)=>(
    //         i === index ? {...item,name:e.target.value} : item
    //     ))
    //     setstate({...state,list:data})
    // }
    // const toggleEdit=(i)=>{
    //     const data=state.list.map((item,index)=>(
    //         i===index ? {...item,edit:! item.edit} : item
    //     ))
    //     setstate({...state,list:data})
    // }
    // const deleteTodo=(i)=>{
    //     // const data=
    //     setstate({...state,list:state.list.filter((item,index)=>i !== index)})
    // }


// const data = [1, 2, 3, 4, 5]

// const forData = data.forEach((item, i) => console.log(item))




// let bdata = [{ a: '' }, { b: '' }]
// const gdata = [{ b: 'sdf' }]
// const ques = bdata.map(item => Object.keys(item)[0])

// gdata.forEach(item => {
//     const q = Object.keys(item)[0]
//     const index = ques.indexOf(q)
//     if (ques.includes(q)) {
//         bdata[index][q] = item[q]
//     }
// })


// // gdata.forEach(item => {
// //   const q = Object.keys(item)[0]
// //   const index = ques.indexOf(q)
// //   console.log(index)
// //   if (ques.includes(q)) {
// //     bdata[index][q] = item[q]
// //   }
// // })

// console.log(bdata)

const data = [1,2,3,4,5]

const forData = data.forEach((item,i)=>console.log(item))




  let bdata = [{ a: '' }, { b: '' }]
  const gdata = [{ b: 'sdf' }]
  const ques = bdata.map(item => Object.keys(item)[0])

  gdata.forEach(item => {
    const q = Object.keys(item)[0]
    const index = ques.indexOf(q)
    if (ques.includes(q)) {
      bdata[index][q] = item[q]
    }
  })

//   console.log(dataAcquisitionpolls[0].questions)



  // gdata.forEach(item => {
  //   const q = Object.keys(item)[0]
  //   const index = ques.indexOf(q)
  //   console.log(index)
  //   if (ques.includes(q)) {
  //     bdata[index][q] = item[q]
  //   }
  // })

  console.log(bdata)
// const openFileManager = () => {
//     ImagePicker.openPicker({
//         width: 300,
//         height: 400,
//         includeBase64: true
//     }).then(image => {
//         const updateImageApicalled = async () => {
//             setLoading(true);
//             setLoaderMessage('loading...');
//             const name = image?.path.split('/')
//             const details = {
//                 "name": `${name[name.length - 1]}`,
//                 "type": `${image.mime}`,
//                 "encoding": "base64",
//                 'imageData': `data:image/png;base64,${image.data}`
//             }
//             try {
//                 const data = await uploadImage(details)
//                 if (data) {
//                     await UpdateMemberbyId({ userImage: data })
//                     await getMemberbyIdApiCall()
//                     setLoading(false);
//                 }
//             } catch (error) {
//                 console.log(error)
//                 setLoading(false);
//             }
//         }
//         updateImageApicalled()
//     });
// }