import React, { useState } from 'react'
import { Text, View, StyleSheet, TextInput, TouchableOpacity } from 'react-native'
import { NAVIGATION } from '../../Constants/navigations'
import lodash from 'lodash-contrib'
const Country = ({ navigation }) => {

  const [country, setcountry] = useState('')
  return (
    <View style={styles.body}>
      <View style={styles.wrapper}>
        <View>
          <Text style={styles.heading}>Country </Text>
          <TextInput value={country}
            onChangeText={e => {
              let value = e
              value = value.split('')
              value = value.map(element => {
                console.log({ element, number: lodash.isNumeric(element) })
                if (lodash.isNumeric(element)) return ''
                return element
              })
              value = value.join('')
              setcountry(value);
            }
            }
            style={styles.input}
          />
        </View>
        <View>
          <Text style={styles.heading}>Language</Text>
          <TextInput style={styles.input}
          />
        </View>
      </View>
      <View>
        <TouchableOpacity onPress={() => navigation.navigate(NAVIGATION.LOGIN)}>
          <View style={{
            backgroundColor: 'red',
            alignItems: 'center',
            justifyContent: 'center',
            height: 45,
            width: 400
          }}
          >
            <Text style={{ color: 'white', fontWeight: '600', fontSize: 19 }}>CONTINUE</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  )
}
const styles = StyleSheet.create({
  body: {
    flex: 1,
    alignItems: 'center'
  },
  wrapper: {
    flex: 1,
    width: 200,
    alignItems: 'center'
  },
  input: {
    borderWidth: 1,
    width: 350,
    borderColor: 'black',
    fontSize: 18, color: 'red'
  },
  heading: {
    fontWeight: '500',
    fontSize: 20,
    padding: 5
  },
  btn: {
  },
  btnstyle: {
    backgroundColor: 'red',
    padding: 10,
    width: 300,
  }
})

export default Country


