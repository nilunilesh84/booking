import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, Button, FlatList,
   TouchableOpacity, TextInput, Image, Dimensions, ScrollView } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Calender from '../Calender/Calender';
import LinearGradient from 'react-native-linear-gradient';
import carosal1 from '../../../assets/carosal';
import carosal2 from '../../../assets/carosal2';
import services from '../../../assets/data'
import { useSelector } from 'react-redux';
import { STYLES } from '../../Constants/theme';

const { width, height } = Dimensions.get('window')

export default function Home({ navigation }) {
  const { city, destination } = useSelector(state => state.Reducer)
  useEffect(() => {

  }, [city, destination])
  const [loading, setloading] = useState(true);
   const renderItem = ({ item }) => {
    return <View style={styles.shadowObject}>
      {item.icon}
      <Text style={{ color: 'black', fontSize: 18, margin: 4 }}>{item.name}</Text>
    </View>
  }
  const rendreCarosal = ({ item }) => (
    <View style={styles.imageContainer}>
      <Image resizeMode='cover' style={styles.image} source={item.image} />
    </View>
  )
  return (
    <ScrollView style={styles.body}>
      <LinearGradient 
        start={{ x: 1.0, y: 0.5 }}
        end={{ x: 0.7, y: 1.0 }}
        colors={['rgba(71, 123, 255, 1)', 'rgba(110, 175, 236, 1)']} style={styles.header}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
          <Text style={styles.headerfont}>Bus tickets</Text>
          <FontAwesome5 style={{
            color: 'white', fontSize: 30,
            padding: 8,
          }} name="bus" />
        </View>
        <View style={styles.inputWrapper}>
          <View style={{ flexDirection: 'row', padding: 4 }}>
            <FontAwesome5 style={styles.icons} name="bus" />
            <TextInput style={styles.input}
              placeholderTextColor="gray"
              onFocus={() => navigation.navigate('CitySearch')}
              placeholder='ENTER SOURCE' value={city} />
          </View>
          <View style={{ flexDirection: 'row', borderTopColor: 'gray', borderTopWidth: 1, padding: 4 }}>
            <FontAwesome5 style={styles.icons} name="bus" />
            <TextInput style={styles.input}
              placeholderTextColor="gray"
              placeholder='ENTER DESTINATION'
              onFocus={() => navigation.navigate('CitySearch2')}
              value={destination} />
          </View>
          <Calender />
          <View style={{ width: 350, alignSelf: 'center', padding: 12 }}>
            <Button  color='red' title='Search' onPress={()=>console.log('sdf')} />
          </View>
        </View>
      </LinearGradient>
      <View >
        <Text style={{ fontSize: 25, fontWeight: '500', alignSelf: 'center' }}
        >You can Also Book</Text>
        <View >
          <View style={{ marginVertical: 10 }}>
            <FlatList
              data={services}
              renderItem={renderItem}
              horizontal
              keyExtractor={item => item.id}
              showsHorizontalScrollIndicator={false}
            />
          </View>
          <View style={{ marginVertical: 10 }}>
            <FlatList
              data={carosal1}
              renderItem={rendreCarosal}
              keyExtractor={item => item.id}
              horizontal
              showsHorizontalScrollIndicator={false}
            />
          </View>
        </View>
        <View>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <TouchableOpacity onPress={() => console.log('hello all')}>
              <Text style={{ fontSize: 18, fontWeight: '500', color: 'black', padding: 5 }}>OFFERS</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => console.log('hello all')}>
              <Text style={{ fontSize: 18, fontWeight: '500', color: 'blue', padding: 5 }}>VIEW ALL</Text>
            </TouchableOpacity>
          </View>
          <FlatList
            data={carosal2}
            renderItem={rendreCarosal}
            keyExtractor={item => item.id}
            horizontal
            showsHorizontalScrollIndicator={false}
          />
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
    height: height
  },
  header: {
    padding: 15,
    height: 300,
    width: width,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  headerfont: {
    color: 'white',
    fontSize: 30,
    paddingRight: 55,
    fontWeight: 'bold'
  },
  iconswrapper: {
    justifyContent: 'center',
    flexDirection: 'row',
  },
  icons: {
    color: 'black',
    fontSize: 30,
    padding: 13,
  },
  input: {
    fontSize: 20,
    padding: 6,
    color: 'black'
  },
  inputWrapper: {
    backgroundColor: 'white',
    height: 230,
    width: '98%',
    borderRadius: 5
  },
  shadowObject: {
    width: width / 2,
    height: 90,
    ...STYLES,
    margin: 2,
    backgroundColor: '#ffff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.41,
    shadowRadius: 9.11,
    elevation: 8,
  },
  image: {
    width: 220,
    height: 130,
    borderRadius: 8,
    margin: 4,
  },
  imageContainer: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.41,
    shadowRadius: 9.11,
    elevation: 18,
  }

})

