import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Bus from '../../../assets/bus.jpg';
import { COLORS, SIZES } from '../../Constants/theme';
import { NAVIGATION } from '../../Constants/navigations';

function LoginScreen({navigation}) {
  // const navigation = useNavigation();
  const [userName, setUserName] = useState('M0000002');
  const [password, setPassword] = useState('johndoe1');
  const [loading, setLoading] = useState(false);
  const [loaderMessage, setLoaderMessage] = useState('');


 

  return (
    <View style={styles.container}>
      <View style={{height: '30%', backgroundColor: 'black'}}>
        <View
          style={{
            justifyContent: 'center',
            alignSelf: 'center',
            height: '100%',width:'100%'
          }}>
          <Image
            source={Bus}
            resizeMode={'cover'}
            style={{width: '100%', height: 130, aspectRatio: 1}}
          />
        </View>
      </View>
      <View
        style={{
          height: '70%',
          backgroundColor:COLORS.white,
          borderTopLeftRadius: 100,
        }}>
        <View style={{justifyContent: 'center', alignSelf: 'center'}}>
          <Text
            style={{
              fontSize: 30,
              fontWeight: '600',
              fontFamily: 'Quicksand',
              color: 'black',
              marginTop: 20,
              marginBottom: 20,
            }}>
            Login
          </Text>
        </View>
        <View style={{marginHorizontal: 40}}>
          <View style={styles.loginContainer}>
            <View style={{margin: 3}}>
              <View style={{marginTop: 10, marginLeft: 10}}>
                <Text style={styles.userNameText}>User Name</Text>
              </View>
              <TextInput
                style={styles.userNameInput}
                placeholder="Enter User Name"
                placeholderTextColor={COLORS.white}
                returnKeyType={'next'}
                onChangeText={text => setUserName(text.trim())}
                autoCapitalize="none"
              />
            </View>
          </View>
          <View style={{marginTop: 20}} />
          <View style={styles.loginContainer}>
            <View style={{margin: 3}}>
              <View style={{marginTop: 10, marginLeft: 10}}>
                <Text style={styles.userNameText}>Password</Text>
              </View>
              <TextInput
                secureTextEntry={true}
                style={styles.userNameInput}
                placeholder="Enter Password"
                placeholderTextColor={COLORS.white}
                returnKeyType={'next'}
                onChangeText={text => setPassword(text.trim())}
                autoCapitalize="none"
              />
            </View>
          </View>
          <View style={{marginTop: 20}} />
          <View style={{justifyContent: 'center', alignSelf: 'flex-end'}}>
            <Text style={{color: COLORS.black, fontSize: 12, fontWeight: '600'}}>
              Forget Password?
            </Text>
          </View>
          <View style={{marginTop: 20}} />
          <TouchableOpacity
            onPress={() => navigation.navigate(NAVIGATION.HOME_STACK)}
            style={{backgroundColor:COLORS.black, borderRadius: 10}}>
            <View>
              <Text
                style={{
                  fontSize: 13,
                  textAlign: 'center',
                  textAlignVertical: 'center',
                  fontWeight: '600',
                  margin: 15,
                  color: 'white',
                }}>
                Login
              </Text>
            </View>
          </TouchableOpacity>
          <View style={{marginTop: 20}} />
          <View style={{justifyContent: 'center', alignSelf: 'center'}}>
            <Text style={{color: COLORS.black, fontSize: 12, fontWeight: '400'}}>
              Don’t have an account?{' '}
              <Text style={{color: COLORS.black, fontWeight: '700'}}>
                Sign Up
              </Text>
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
}

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    height: SIZES.height,
  },
  loginContainer: {
    elevation: 6,
    backgroundColor: 'white',
    borderRadius: 10,
    shadowColor: COLORS.gray,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 1.27,
    shadowRadius: 4.65,
  },
  userNameText: {
    fontSize: 14,
    fontWeight: '600',
    fontFamily: 'Quicksand',
  },
  userNameInput: {
    width: '100%',
    color: COLORS.gray,
    padding: 10,
    fontFamily: 'Quicksand',
  },
});
