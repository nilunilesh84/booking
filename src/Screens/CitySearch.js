import { StyleSheet, Text, View } from 'react-native';
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { TextInput } from 'react-native-paper';
import { citySave } from '../redux/Actions/action';
import { NAVIGATION } from '../Constants/navigations';

const CitySearch = ({ navigation, placeholder }) => {
  const [destination, setDestination] = useState('')
  const dispatch = useDispatch()
  return (
    <View >
      <TextInput style={styles.input}
        onEndEditing={() => (dispatch(citySave(destination)), navigation.navigate(NAVIGATION.HOME))}
        placeholderTextColor="gray" placeholder='ENTER CITY'
        value={destination} onChangeText={(value) => setDestination(value)} />
    </View>
  );
};

export default CitySearch;

const styles = StyleSheet.create({

});
