import { View,ActivityIndicator } from 'react-native';
import React from 'react';
import LottieView from 'lottie-react-native';

const CustomLoader = () => {
    return (
        <View style={{
            flex: 1,
            // backgroundColor: COLORS.background,
            justifyContent: 'center',
            alignItems: 'center'
        }}>
           {/* <ActivityIndicator /> */}
           <LottieView
        source={require('../../assets/44290-double-decker-bus-running.json')}
        colorFilters={[
          {
            keypath: 'button',
            color: '#F00000',
          },
          {
            keypath: 'Sending Loader',
            color: '#F00000',
          },
        ]}
        autoPlay
        loop
      />
        </View>
    );
};

export default CustomLoader;
