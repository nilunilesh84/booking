import { Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");

export const COLORS = {
    // base colors
    primary: '#38354B',
    secondary: '#1A2C50',
    background: '#201E2D',
    // colors
    black: "#000000",
    white: "#FFFFFF",
    //others
    transparent: "transparent",
    darkgray: '#898C95',
};

export const STYLES = {
    justifyContent: 'center', 
    alignItems: 'center'
}

export const SIZES = {
    // global sizes
    base: 8,
    font: 14,
    radius: 30,
    padding: 10,
    padding2: 12,

    // font sizes
    largeTitle: 50,
    h1: 30,
    h2: 22,
    h3: 20,
    h4: 18,
    h5: 14,

    // app dimensions
    width,
    height
};

export const FONTS = {
    h1: { fontFamily: "Quicksand", fontSize: SIZES.h1, lineHeight: 36 },
    h2: { fontFamily: "Quicksand", fontSize: SIZES.h2, color: COLORS.black, fontWeight: '500' },
    h3: { fontFamily: "Quicksand", fontSize: SIZES.h3, color: COLORS.black, fontWeight: '500' },
    h4: { fontFamily: "Quicksand", fontSize: SIZES.h4, color: COLORS.black, fontWeight: '500' },
    h5: { fontFamily: "Quicksand", fontSize: SIZES.h5, color: COLORS.black, fontWeight: '500' },
};

const appTheme = { COLORS, SIZES, FONTS };

export default appTheme;