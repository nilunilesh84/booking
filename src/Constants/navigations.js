

export const NAVIGATION={
    COUNTRY:'Country',
    LOGIN:'Login',
    REGISTER:'Register',
    CITYSEARCH:'CitySearch',
    CITYSEARCH1:'CitySearch2',
    HOME:'Home',
    MY_BOOKINGS:'MyBookings',
    HELP:'Help',
    MY_ACCOUNT:'MyAccount',
    TALK:'Talk',
    HOME_STACK:'Homepage',
}