import { createStore, applyMiddleware, combineReducers } from "redux";
import thunk from "redux-thunk";
import UserReducers from './Reducer/UserReducers'
import Reducer from "./Reducer/Reducer.js";
import loading from './Reducer/loaderreducer'
const rootReducer = combineReducers({
    Reducer,
    user: UserReducers,
    loading
})

export const Store = createStore(rootReducer, applyMiddleware(thunk))