 import { SHOW_LOADER ,HIDE_LOADER} from "../Actions/loaderAction"

const initState = {
    loadingsms: false 
}

export default function Reducer(state = initState, action) {
    const { type, payload } = action
    switch (type) {
        case SHOW_LOADER:
            return { ...state, loadingsms: true }
        case HIDE_LOADER:
            return { ...state, loadingsms: false }
        default:
            return state

    }
}