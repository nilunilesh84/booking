/* eslint-disable prettier/prettier */

import {
    SAVE_CITY,SAVE_DESTINATION
} from "../Actions/action";


const initState = {
    city: null,
    destination:null
}

export default function Reducer(state = initState, action) {
    const { type, payload } = action
    switch (type) {
        case SAVE_CITY:
            return { ...state, city: payload }
        case SAVE_DESTINATION:
            return { ...state, destination: payload }
        default:
            return state
    }
}