/* eslint-disable prettier/prettier */
export const SAVE_CITY = 'SAVE_CITY';
export const SAVE_DESTINATION = 'SAVE_DESTINATION';

export const citySave = (data) => dispatch => {
    dispatch({ type: SAVE_CITY, payload: data })
}
export const destinationSave = (data) => dispatch => {
    dispatch({ type: SAVE_DESTINATION, payload: data })
}