import { View, Text } from 'react-native'
import React from 'react'
import HomePageStack from './BottomNavigator/BottomNavigator'
import Country from './Screens/Country';
import Login from './Screens/Login';
import CitySearch from './Screens/CitySearch'
import CitySearch2 from './Screens/CitySearch2'
import Register from './Screens/Register';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NAVIGATION } from './Constants/navigations'
const screenOptionStyle = {
    headerStyle: {
        backgroundColor: '#9AC4F8',
    },
    headerTintColor: 'white',
    headerBackTitle: 'Back',
    headerShown: false,
};
const AppNavigation = (token) => {


    const Stack = createNativeStackNavigator();

    const WelcomeStack = () => {
        return (
            <Stack.Navigator screenOptions={{headerShown:false}} >
                <Stack.Screen name={NAVIGATION.COUNTRY} component={Country} />
                <Stack.Screen name={NAVIGATION.LOGIN} component={Login} />
                <Stack.Screen name={NAVIGATION.CITYSEARCH} component={CitySearch} />
                <Stack.Screen name={NAVIGATION.CITYSEARCH1} component={CitySearch2} />
                <Stack.Screen name={NAVIGATION.REGISTER} component={Register} />
                <Stack.Screen name={NAVIGATION.HOME_STACK} component={HomePageStack} />
            </Stack.Navigator>
        )
    }

    return (
        <NavigationContainer screenOptions={screenOptionStyle}>
            {/* {
                token !== null ?  : 
            } */}
            <WelcomeStack />
            {/* <HomePageStack /> */}
        </NavigationContainer>
    )
}

export default AppNavigation