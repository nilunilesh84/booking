import React from 'react';
import Home from '../Screens/Home';
import Myaccount from '../Screens/MyAccount';
import Talk from '../Screens/Talk';
import Bookings from '../Screens/Bookings'


import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NAVIGATION } from '../Constants/navigations';


const Bottom = createBottomTabNavigator();

export default function App() {

  return (
    <Bottom.Navigator screenOptions={({ route }) => ({
      header: () => null,
      tabBarIcon: ({ focused, color, size }) => {
        let iconName;
        if (route.name === NAVIGATION.HOME) {
          iconName = 'home';
        } else if (route.name === NAVIGATION.MY_BOOKINGS) {
          iconName = 'list';
        } else if (route.name === NAVIGATION.TALK) {
          iconName = 'headset';
        } else if (route.name === NAVIGATION.MY_ACCOUNT) {
          iconName = 'user-circle';
        }
        return <FontAwesome5 name={iconName} size={focused ? 26 : 23} color={color} />;
      },
      tabBarActiveTintColor: 'tomato',
      tabBarInactiveTintColor: 'gray',
      tabBarLabelStyle: { fontSize: 13 }
    })}>
      <Bottom.Screen name={NAVIGATION.HOME} component={Home} />
      <Bottom.Screen name={NAVIGATION.MY_BOOKINGS} component={Bookings} />
      <Bottom.Screen  options={{
        tabBarLabel:'Help'
      }} name={NAVIGATION.TALK} component={Talk} />
      <Bottom.Screen name={NAVIGATION.MY_ACCOUNT} component={Myaccount} />
    </Bottom.Navigator>
  );
}

{/* <Bottom.Screen name={NAVIGATION.TALK} component={Talk} options={{
  tabBarIcon: ({ focused }) => (
    <View style={{ width: 50, height: 50, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center', borderRadius: 25 }}>
      <FontAwesome5 name={'microphone'} size={focused ? 28 : 23} color={focused ? 'tomato' : 'gray'} />
    </View>
  ),
  tabBarButton: (props) => (
    <CustomButton {...props} />
  ),
}} /> */}
// const CustomButton = ({ children, onPress }) => (
//   <TouchableOpacity onPress={onPress}
//     style={{ top: -20, justifyContent: 'center', alignItems: 'center' }}>
//     <View>
//       {children}
//     </View>
//   </TouchableOpacity>
// )