/**
 * @format
 */
import * as React from 'react';
import {AppRegistry} from 'react-native';
import { Provider } from 'react-redux';
import App from './App';
import {Store} from './src/redux/store'
import {name as appName} from './app.json';

export default function Main(){
    return (
        <Provider store={Store}>
            <App/>
        </Provider>
    )
}

AppRegistry.registerComponent(appName, () => Main);
